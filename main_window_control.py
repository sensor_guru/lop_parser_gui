import autogen_mainwindow
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
import pyqtgraph as pg
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterItem, registerParameterType
from pyqtgraph.dockarea import *
import sys
import requests
import numpy as np
import pandas as pd
import lop_parser as lop

# ===================
# EXAMPLES AND DEMOS 
#====================
# from bash (or ipython env)
# >>> python
# >>> import pyqtgraph.examples
# >>> pyqtgraph.examples.run()


class myGUI(QtWidgets.QMainWindow, autogen_mainwindow.Ui_MainWindow):

    def __init__(self):
        """ Initialize myGUI object"""
    
        # ==============================
        # Begin required content
        # ================================
        QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)
        # ================================
        # End required content
        # ===============================

        # ===============================
        # Begin custom content
        # ================================
          

        # --------------------------------------------------------
        # assign functions that are called when a button is pushed
        #---------------------------------------------------------
        
        self.btn_filter.clicked.connect(self.filter_wavelength)
    
        self.menu_load_file.triggered.connect(self.load_file)
        
        #----------------------------
        # parameter tree
        #----------------------------
        pt = self.treeWidget

        params = [
            {'name': 'SECTION', 'type': 'group', 'children': [
                {'name': 'Named_List', 'type': 'list', 'values': {"DHR": "{DHR}", "DDHR": "{DDHR}", "DHT": "{DHT}", "DDHT": "{DDHT}",
                                                                 "HDR": "{HDR}", "DDR": "{DDR}","HDT": "{HDT}", "DDT": "{DDT}", "BRDD": "{BRDD}" }, 'value': "{DHR}"},
            ]},
            {'name': 'INFORMATIONAL TAGS', 'type': 'group', 'children': [
                {'name': 'Instrument', 'type': 'list', 'values': {"SOC-100": 0, "SOC-410": 1, "Pike IntegratIR + FTIR": 2, "Ocean Optics + Maya": 3, "Seagull + FTIR": 4,}, 'value': 4},
                {'name': 'Detector', 'type': 'list', 'values': {"MCT": 0, "Si": 1,}, 'value': 1},
                {'name': 'Source', 'type': 'list', 'values': {"Thermal": 0, "Laser": 1,}, 'value': 2},
                {'name': 'Beamsplitter', 'type': 'list', 'values': {"KBr": 0, "xxx": 1,}, 'value': 2},
            ]},

            {'name': 'CONFIGURATION TAGS', 'type': 'group', 'children': [
                {'name': 'Count', 'type': 'int', 'value': 10},
                {'name': 'IncidentPol', 'type': 'int', 'value': -1},
                {'name': 'SampRotAng', 'type': 'int', 'value': 0},
                {'name': 'SampTemp', 'type': 'int', 'value': 22},
                {'name': 'AnlysPol', 'type': 'int', 'value': -1},
                {'name': 'SourcePol', 'type': 'int', 'value': -1},
                {'name': 'SourceAzimuthAng', 'type': 'int', 'value': 0},
                {'name': 'SourceIncidentAng', 'type': 'int', 'value': 30},
                {'name': 'DetectAzimuthAng', 'type': 'int', 'value': 180},
            ]},

            {'name': 'WAVELENGTH', 'type': 'group', 'children': [
                {'name': 'Wavelength_1', 'type': 'float', 'value': .6e-6, 'step': 1e-6, 'siPrefix': True, 'suffix': 'm'},
                {'name': 'Wavelength_2', 'type': 'float', 'value': 1.5e-6, 'step': 1e-6, 'siPrefix': True, 'suffix': 'm'},
                {'name': 'Limits (min=7;max=15)', 'type': 'int', 'value': 11, 'limits': (7, 15), 'default': -6},
                {'name': 'Range', 'type': 'list', 'values': {"Over": 0, "Under": 1, "Nearest": 2, "Range": 3}, 'value': 3},
                
            ]},

            {'name': 'Save/Restore functionality', 'type': 'group', 'children': [
                {'name': 'Save State', 'type': 'action'},
                {'name': 'Restore State', 'type': 'action', 'children': [
                    {'name': 'Add missing items', 'type': 'bool', 'value': True},
                    {'name': 'Remove extra items', 'type': 'bool', 'value': True},
                ]},

            ]}]
            
        # Create tree of Parameter objects
        self.params = Parameter.create(name='params', type='group', children=params)

        # store the inital state so we can always reset
        self.params_initstate = self.params.saveState()
        pt.setParameters(self.params, showTop=True)

        params_nowstate = self.params.saveState()
        params_od = params_nowstate['children']
        self.wavelength1 = params_od['WAVELENGTH']["children"]["Wavelength_1"]["value"]
        self.wavelength2 = params_od['WAVELENGTH']["children"]["Wavelength_2"]["value"]
        self.section = params_od['SECTION']["children"]["Named_List"]["value"]

        ## If anything changes in the tree, print a message
        def change(param, changes):
            print("tree changes:")
            for param, change, data in changes:
                path = self.params.childPath(param)
                if path is not None:
                    childName = '.'.join(path)
                else:
                    childName = param.name()
                print('  parameter: %s'% childName)
                print('  change:    %s'% change)
                print('  data:      %s'% str(data))
                print('  ----------')
            
        self.params.sigTreeStateChanged.connect(change)


        def valueChanging(param, value):
            print("Value changing (not finalized): %s %s" % (param, value))
            
        # Too lazy for recursion:
        for child in self.params.children():
            child.sigValueChanging.connect(valueChanging)
            for ch2 in child.children():
                ch2.sigValueChanging.connect(valueChanging)
                


        def save():
            global state
            state = self.params.saveState()
            
        def restore():
            global state
            add = self.params['Save/Restore functionality', 'Restore State', 'Add missing items']
            rem = self.params['Save/Restore functionality', 'Restore State', 'Remove extra items']
            self.params.restoreState(state, addChildren=add, removeChildren=rem)
        self.params.param('Save/Restore functionality', 'Save State').sigActivated.connect(save)
        self.params.param('Save/Restore functionality', 'Restore State').sigActivated.connect(restore)


        ## Create two ParameterTree widgets, both accessing the same data
        pt.setWindowTitle('pyqtgraph example: Parameter Tree')

        ## test save/restore
        s = self.params.saveState()
        self.params.restoreState(s)


        

        # ================================
        # ENd custom content
        # ================================

    #=====================================================
    #               END OF INIT FUNCTION
    #=====================================================
    
    # --------------------------------
    # DEFENITIONS THAT UPDATE GUI WIDGETS
    # --------------------------------

    # Load in lop file (csv) to be filtered
    def load_file(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        if dlg.exec_():
            filename = dlg.selectedFiles()
            self.df = pd.read_csv(filename[0], low_memory=False)
            self.df.replace(np.nan,"",inplace=True)
            self.populate_data_docker(self.df, True)
            return

    # Filters Dataframe based on wavelength values in parametree
    def filter_wavelength(self):
        params_nowstate = self.params.saveState()
        params_od = params_nowstate['children']
        self.wavelength1 = params_od['WAVELENGTH']["children"]["Wavelength_1"]["value"]
        self.wavelength2 = params_od['WAVELENGTH']["children"]["Wavelength_2"]["value"]
        self.section = params_od['SECTION']["children"]["Named_List"]["value"]
        
        print("min={0}, max={1}".format(self.wavelength1, self.wavelength2))
        df_organized = lop.section_sort(self.df, self.section)
        df_filtered = lop.criterion(df_organized, self.wavelength1, self.wavelength2)
        self.populate_data_docker(df_filtered)
        return


    def populate_data_docker(self, df, inital_load=False):
        # ---------------------------------------
        # Tabbed Docker for filtered data
        # ---------------------------------------
          
        area = DockArea()
        self.setCentralWidget(area)
        #d0 = Dock("(Experiment 1)")
        #d1 = Dock("(Experiment 2)")

        #area.addDock(d0, 'right', realitiveTo=None)     ## place d0 at right edge of dock area
        #area.addDock(d1, 'above', d0)   ## place d1 at top edge of d0
        '''
        if inital_load ==True:
            d = Dock("Lop File Aggegrate")
            area.addDock(d, 'above', relativeTo=None)
            w = pg.TableWidget()
            w.setData(np.asarray(df))
            d.addWidget(w)
            return
        else:
            '''
        if  inital_load == True:
            d  = Dock("Experiment tab ")
            area.addDock(d, 'above', relativeTo=None)
            w = pg.TableWidget()
            d.addWidget(w)
            return
        else:

            w = []
            d = []
            for i in range(len(df)):
                d  += [Dock("Experiment tab " + str(i))]
                area.addDock(d[i], 'above', relativeTo=None)
                w +=[pg.TableWidget()]
                w[i].setData(np.asarray(df[i]))
                d[i].addWidget(w[i])
        return



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    form = myGUI()
    form.show()
    sys.exit(app.exec())
    #app.exec_()