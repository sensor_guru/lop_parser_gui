# %%
# -*- coding: utf-8 -*-
# LOP FILE PARSER

### Created By : Jesse Terrel-Perez

### Created       : 02March2021

### Last Edited : 03March2021
# %%

## TOC:
#* [Methods](#methods)
#* [Loading Data](#load_data)
#%%

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
# %%

### METHODS  <a class="anchor" id="methods"></a>


def section_sort(df, section):
    """
    Finds the requested section in the lop file and returns the experiments in that
    section. Additonally, it reorgainizes each experiment to follow the pattern:
     * \# = Information tag
     * & = Configuration tag
     * data
     
     Parameters
     ----------
     df : pandas.core.series.dataFrame
          pandas dataframe.
     
     
    """
    section_index     = np.asarray(df.iloc[:,0][df.iloc[:,0] == section].index.tolist())
    total_start_index = np.asarray(df[section_index[0] : ].iloc[:,0][df.iloc[:,0] == r'[Start]'].index.tolist())
    total_end_index   = np.asarray(df[section_index[0] : ].iloc[:,0][df.iloc[:,0] == r'[End]'].index.tolist())
    config_tags = np.asarray(df.iloc[:,0][df.iloc[:,0].str.contains(r'&',na=False)].index.tolist())
    info_tags   = np.asarray(df.iloc[:,0][df.iloc[:,0].str.contains(r'#',na=False)].index.tolist())
    
    section_start = []
    section_end = []

    for i in range(section_index.size):
        section_start += [total_start_index[np.nonzero((total_start_index > section_index[i]))[0][0]]]
        section_end += [total_end_index[np.nonzero((total_end_index > section_index[i]))[0][0]]]

        
    #=============================================================================================
    # Return a list of Sorted and reorganized dataFrames that are part of the selected section
    #=============================================================================================
    
    # Seperates all the data ([Start] : [End]) for the given section
    frames_data = []
    for i in range(len(section_start)):    
        frames_data += [df.iloc[section_start[i]: section_end[i]+1]]
    # Seperates all the config and info comments for a given experiment in the selected section
    frames_config = []
    frames_info   = []
    for i in range(len(section_index)):
        frames_config += [df.iloc[config_tags[(config_tags > section_index[i]) & (config_tags < section_end[i])]]]
        frames_info   += [df.iloc[info_tags[(info_tags > section_index[i]) & (info_tags < section_end[i])]]]
    
    #=============================================================================================
    # Reorganizing and seperating data for seperate experimental runs
    #=============================================================================================
    
    seperated_experiments = []
    for i in range(len(section_start)):
        seperated_experiments += [df.iloc[frames_info[i].iloc[:].index.tolist() +
                                          frames_config[i].iloc[:].index.tolist() + 
                                          frames_data[i].iloc[:].index.tolist()].reset_index(drop=True)]
        
    # Finds all the indexes where the info tags (#) and configuration tags (&) are located
    #info_tags   = np.asarray(np.where(np.array(df.iloc[:,0].map(lambda x: str(x).startswith('#')))))
    #config_tags = np.asarray(np.where(np.array(df.iloc[:,0].map(lambda x: str(x).startswith('&')))))

    return seperated_experiments
    #return section_index, np.asarray(section_start), np.asarray(section_end), total_config_tags, total_info_tags

    
def experiment_count(sorted_experiments):
    '''Returns number of experiments fitting criteria'''
    return len(sorted_experiments)


def criterion(df_list, wave_min = 0, wave_max = 0 ):
    wave_min = wave_min * 1e6
    wave_max = wave_max * 1e6
    df_filt = []
    wave_filter = []
    sorted_info_tags   = []
    sorted_config_tags = []
    start_index = []
    end_index = []

    for i in range(len(df_list)):
        sorted_info_tags   += [df_list[i].iloc[:,0][df_list[i].iloc[:,0].str.contains(r'#',na=False)].index.tolist()]
        sorted_config_tags += [df_list[i].iloc[:,0][df_list[i].iloc[:,0].str.contains(r'&',na=False)].index.tolist()]
        start_index        += [df_list[i].iloc[:,0][df_list[i].iloc[:,0] == r'[Start]'].index.tolist()]
        end_index          += [df_list[i].iloc[:,0][df_list[i].iloc[:,0] == r'[End]'].index.tolist()]
    
    if(wave_min == wave_max):
        #return nearest wavelength
         for i in range(len(df_list)):
            wavelength = find_nearest(df_list[i][start_index[i][0]+2: end_index[i][0]].iloc[:,0], wave_min)
            wave_filter += [df_list[i][start_index[i][0]+2 : end_index[i][0]]
                            [(df_list[i][start_index[i][0]+2 : end_index[i][0]].iloc[:,0].astype(float) != wavelength)].index.tolist()]
            
    elif(wave_min != 0 and wave_max != 0):      
        # Find all indexes (wavelength) in dataframes that are outsid requested wavelength range
        for i in range(len(df_list)):
            wave_filter += [df_list[i][start_index[i][0]+2 : end_index[i][0]]
                            [(df_list[i][start_index[i][0]+2 : end_index[i][0]].iloc[:,0].astype(float) < wave_min) |
                             (df_list[i][start_index[i][0]+2 : end_index[i][0]].iloc[:,0].astype(float) > wave_max)].index.tolist()]
             
    elif(wave_min == 0 and wave_max != 0):
        #filter out all wavelengths below maximum wavelength
        # Find all indexes (wavelength) in dataframes that are outsid requested wavelength range
        for i in range(len(df_list)):
            wave_filter += [df_list[i][start_index[i][0]+2 : end_index[i][0]]
                            [(df_list[i][start_index[i][0]+2 : end_index[i][0]].iloc[:,0].astype(float) < wave_max)].index.tolist()]               
    
    elif(wave_min != 0 and wave_max == 0):
        #filter out all wavelengths above minimum wavelength
         for i in range(len(df_list)):
            wave_filter += [df_list[i][start_index[i][0]+2 : end_index[i][0]]
                            [(df_list[i][start_index[i][0]+2: end_index[i][0]].iloc[:,0].astype(float) > wave_min)].index.tolist()]
                
    #drop all row indexes that are not within specified wavelength and rebuild dataframes
    for i in range(len(df_list)):
        df_filt += [df_list[i].drop(wave_filter[i]).reset_index(drop=True)]
 
    return df_filt

def find_nearest(array, value):
    array = np.asarray(array).astype(float)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

# %%

### LOAD DATA <a class="anchor" id="load_data"></a>
'''
* Load data
* Get experiments for selected section (e.g. {BRDD}) and have them reorganized as follows:
 * \# = Information tag
 * & = Configuration tag
 * data
'''
# %%

##df = pd.read_csv("DG10_120_M01_OpticalMeas_pandas.csv", low_memory=False)
##df.replace(np.nan,"",inplace=True)

#%%

# Gets all the relavent information in the lop file for the selected section (e.g. {DHR}) returns a list of dataFrames ...
# One for each experiment.{DHR}, {HDR}
# Change 2nd argument for diffent sections
##organized_experiments = section_sort(df, '{DHR}')
#%%
##organized_experiments[0]
#%%

'''Examples of filtering experiments (list of dataframes) by wavelength. Uncomment to try one.
    Note: You may need to change wavelength range depending on which experiments are selected'''
#df_filtered = criterion(organized_experiments, wave_min=5.2) # All wavelenghts below wave_min
#df_filtered = criterion(organized_experiments, wave_max=5.2) # All wavelenghts above wave_min
##df_filtered = criterion(organized_experiments, wave_min=0.4, wave_max=0.59) # wavelengths between wave_min & wave_max
#df_filtered = criterion(organized_experiments, wave_min=5.2, wave_max=5.2) # closest wavelengh (need to be the same)
#%%
##print(df_filtered[0])
# %%

# Example of saveing filtered data in a csv or excel file with diffent sheets



